
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>lazyPanel | A server administration panel, for the real lazy</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Le styles -->
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/font-awesome.min.css" rel="stylesheet">
    <style type="text/css">
      body {
        padding-top: 20px;
        padding-bottom: 40px;
      }

      /* Custom container */
      .container-narrow {
        margin: 0 auto;
        max-width: 700px;
      }
      .container-narrow > hr {
        margin: 30px 0;
      }

      /* Main marketing message and sign up button */
      .jumbotron {
        margin: 60px 0;
        text-align: center;
      }
      .jumbotron h1 {
        font-size: 72px;
        line-height: 1;
      }
      .jumbotron .btn {
        font-size: 21px;
        padding: 14px 24px;
      }

      /* Supporting marketing content */
      .marketing {
        margin: 60px 0;
      }
      .marketing p + h4 {
        margin-top: 28px;
      }

      /* Icon modifications */
      #icon {
        margin-top: 4px;
      }
    </style>
    <link href="/css/bootstrap-responsive.css" rel="stylesheet">

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
  </head>

  <body>

    <div class="container-narrow">

      <div class="masthead">
        <h3 class="muted">lazyPanel</h3>
      </div>

      <hr>

      <div class="jumbotron">
        <h1>The best things in life are free!</h1>
        <p class="lead">And with lazyPanel, we don't change the best things in life. We're a free, open source, simple server management tool, especially for the lazy... like us.</p>
        <a class="btn btn-large btn-primary" href="http://team.lazypanel.com">View on BitBucket.org &raquo;</a>
      </div>

      <hr>

      <h2>So why pick us?</h2>

      <div class="row-fluid marketing">
        <div class="span6">
          <h4><i class="icon-beer"></i> It's free, as in beer!</h4>
          <p>We don't cost anything, nothing. Download and install knowing your wallet has not been affected.</p>

          <h4><i class="icon-globe" id="icon"></i> It's platform independent!</h4>
          <p>Running Windows? Debian? Mac? lazyPanel loves them all!</p>

          <h4><i class="icon-thumbs-up" id="icon"></i> Powered by Django &amp; Python</h4>
          <p>Enough said here really.... we love Python &amp; Django</p>

        </div>

        <div class="span6">
          <h4><i class="icon-eye-open" id="icon"></i> It's open source!</h4>
          <p>They say many hands make light work, with lazyPanel, many eyes make fewer mistakes.</p>

          <h4><i class="icon-comment" id="icon"></i> We're community driven!</h4>
          <p>Our community is at the heart of everything we do... from ideas, to documentation.</p>
        </div>
      </div>

      <hr>

      <div class="footer">
        <p>&copy; lazyPanel 2013</p>
      </div>

    </div> <!-- /container -->

    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="/js/jquery.js"></script>
    <script src="/js/bootstrap.min.js"></script>

    <!-- Piwik --> 
    <script type="text/javascript">
      var pkBaseURL = (("https:" == document.location.protocol) ? "https://a.mgall.me/" : "http://a.mgall.me/");
      document.write(unescape("%3Cscript src='" + pkBaseURL + "piwik.js' type='text/javascript'%3E%3C/script%3E"));
    </script><script type="text/javascript">
    try {
      var piwikTracker = Piwik.getTracker(pkBaseURL + "piwik.php", 7);
      piwikTracker.trackPageView();
      piwikTracker.enableLinkTracking();
    } catch( err ) {}
    </script><noscript><p><img src="http://a.mgall.me/piwik.php?idsite=7" style="border:0" alt="" /></p></noscript>
    <!-- End Piwik Tracking Code -->
  </body>
</html>
